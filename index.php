<?php
ini_set('error_reporting', E_ALL);
error_reporting(-1);

require_once('config.php');

$tpl->draw('overall_header');

function checkAge( $uname ) {
	$filepath = $_SERVER['DOCUMENT_ROOT'] . '/cache/' . $uname . '.txt';
	if (!file_exists($filepath) or time()-filemtime($filepath) > 1 * 3600) {
		$done = file_get_contents('https://osu.ppy.sh/api/get_user?k=' . APIKEY . '&u=' . $uname );
		$file = fopen($filepath, "w");
		fwrite($file, $done);
		fclose($file); 
	} else {
		$done = file_get_contents( $filepath );
	}
	return $done;
}


$uname = 'avail';
$stuff =  json_decode(checkAge($uname), true);
$osuentries = array();
for ( $i = 0; $i < 6; ++$i ) {
	$toreplace = $stuff[0]['events'][$i]['display_html'];
	$replaced = str_replace("<img src='/images/","<img src='//osu.ppy.sh/images/", $toreplace);
	$replacedbeforelast = str_replace("<a href='/b/","<a href='//osu.ppy.sh/b/", $replaced);
	$replacedlast = str_replace("<a href='/u/","<a href='//osu.ppy.sh/u/", $replacedbeforelast);
	array_push($osuentries, array("event" => $replacedlast));
	$tpl->assign("osuentries", $osuentries);
}


$entries = array();
foreach ($feed->get_items($start,$length) as $key=>$item) {
	array_push($entries, array("title" => $item->get_title(),
	                           "date" => $item->get_date(),
	                           "link" => $item->get_link()
							  )
			  );
}
$tpl->assign("entries", $entries);
$tpl->draw('content');

$tpl->draw('overall_footer');
?>